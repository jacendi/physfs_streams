#ifndef PHYSFS_STREAMS_HPP
#define PHYSFS_STREAMS_HPP

#include <physfs.h>
#include <memory>
#include <vector>
#include <stdexcept>
#include <istream>
#include <ostream>

namespace PhysFS
{
    // RAII wrapper for PHYSFS_file object.
    struct PHYSFS_file_deleter { void operator ()(PHYSFS_file *p) { if (p) PHYSFS_close(p); } };
    using FileWrapper = std::unique_ptr<PHYSFS_file, PHYSFS_file_deleter>;
    /*
     * std::streambuf implementation for PHYSFS_file object.
     */
    class streambuf final : public std::streambuf
    {
    private:
        streambuf(const streambuf&) = delete;
        FileWrapper _file;
        std::vector<char_type> _buf;
    public:
        streambuf(FileWrapper f, size_t buf_size = 1024) : _file(std::move(f)), _buf(buf_size)
        {
            char_type* start = _buf.data();
            char_type* end = _buf.data() + buf_size;

            /*
             * eback() = start, gptr() = end
             * egptr() = end. Because gptr() == egptr(), buffer is empty
             * and will be filled on the first read attempt
             */
            setg(start, end, end);
            setp(start, end);
        }
        virtual ~streambuf() { sync(); }
    protected:
        virtual int_type underflow() override
        {
            if (PHYSFS_eof(_file.get()))
                return traits_type::eof();

            // Read no more than the buffer can hold
            char_type* start = eback();
            auto rd = PHYSFS_readBytes(_file.get(), start, _buf.size());

            setg(start, start, start + rd);

            return rd < 1 ? traits_type::eof()
                          : traits_type::to_int_type(*gptr());
        }
        virtual int_type overflow(int_type __c = traits_type::eof()) override
        {
            // if buffer is empty, then there's nothing
            // to synchronize
            if (pptr() == pbase())
                return 0;

            auto length = static_cast<PHYSFS_uint64>(pptr() - pbase());

            if (PHYSFS_writeBytes(_file.get(), pbase(), length) < 1)
                return traits_type::eof();

            if (__c != traits_type::eof())
            {
                if (PHYSFS_writeBytes(_file.get(), &__c, 1) < 1)
                    return traits_type::eof();
            }

            return 0;
        }
        virtual int sync() override { return overflow(); }
        virtual pos_type seekpos(pos_type pos, std::ios_base::openmode) override
        {
            if (PHYSFS_seek(_file.get(), static_cast<PHYSFS_uint64>(pos)) == 0)
                return pos_type(off_type(-1));

            setg(eback(), eback(), eback());

            return pos;
        }
        virtual pos_type seekoff(off_type off, std::ios_base::seekdir dir,
                                 std::ios_base::openmode mode) override
        {
            pos_type sp = off;

            switch (dir)
            {
            case std::ios_base::beg:
                break;
            case std::ios_base::cur:
            {
                off_type res = static_cast<off_type>(PHYSFS_tell(_file.get()) -
                               static_cast<off_type>(egptr() - gptr()));

                if (off == 0)
                    return res;

                sp += res;
                break;
            }
            case std::ios_base::end:
                sp += static_cast<off_type>(PHYSFS_fileLength(_file.get()));
                break;
            default:
                return pos_type(off_type(-1));
                break;
            }

            return seekpos(sp, mode);
        }
    };
    /*
     * Base class for ease of streambuf construction for
     * both istream and ostream classes and error handling.
     */
    class basic_stream
    {
    protected:
        std::unique_ptr<std::streambuf> sb;
    public:
        basic_stream(const std::string &filename, std::ios_base::openmode mode)
            : sb(nullptr)
        {
            if (filename.empty())
                throw std::runtime_error("Attempt to create PhysFS stream with empty file name!");

            FileWrapper temp = nullptr;

            switch (mode)
            {
            case std::ios_base::app:
            case std::ios_base::app|std::ios_base::out:
                temp.reset(PHYSFS_openAppend(filename.c_str()));
                break;
            case std::ios_base::in:
                temp.reset(PHYSFS_openRead(filename.c_str()));
                break;
            case std::ios_base::out:
                temp.reset(PHYSFS_openWrite(filename.c_str()));
                break;
            default:
                break;
            }

            if (!temp)
                throw std::runtime_error("Couldn't open file '" + filename + "'.");

            sb.reset(new streambuf(std::move(temp)));
        }
        virtual ~basic_stream() {}
    };
    /*
     * These classes implement std::istream and std::ostream objects
     * for PhysFS files.
     */
    class istream final : public basic_stream, public std::istream
    {
    private:
        istream(const istream&) = delete;
    public:
        istream(const std::string &filename) : basic_stream(filename, std::ios_base::in),
            std::istream(sb.get()) {}
    };
    class ostream final : public basic_stream, public std::ostream
    {
    private:
        ostream(const ostream&) = delete;
    public:
        ostream(const std::string &filename, std::ios_base::openmode mode = std::ios_base::out)
            : basic_stream(filename, mode &= ~std::ios_base::in), std::ostream(sb.get()) {}
    };
}

#endif // PHYSFS_STREAMS_HPP
